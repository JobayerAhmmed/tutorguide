package com.munshi.repository;

import com.munshi.model.Class;
import com.munshi.model.Medium;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassRepository extends JpaRepository<Class, Long> {
    List<Class> findByMedium(Medium medium);
}
