package com.munshi.repository;

import com.munshi.model.PermanentAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermanentAddressRepository extends JpaRepository<PermanentAddress, Long> {
}
