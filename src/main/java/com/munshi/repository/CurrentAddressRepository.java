package com.munshi.repository;

import com.munshi.model.CurrentAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrentAddressRepository extends JpaRepository<CurrentAddress, Long> {
}
