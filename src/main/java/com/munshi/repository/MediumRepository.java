package com.munshi.repository;

import com.munshi.model.Medium;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediumRepository extends JpaRepository<Medium, Long> {
}
