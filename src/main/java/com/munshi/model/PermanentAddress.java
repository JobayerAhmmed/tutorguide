package com.munshi.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "permanent_address")
public class PermanentAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "permanent_address_id")
    private Long id;

    @Size(max = 30, message = "Should not exceed 30 letters")
    private String village;

    @Size(max = 30, message = "Should not exceed 30 letters")
    @Column(name = "post_office")
    private String postOffice;

    @Size(max = 30, message = "Should not exceed 30 letters")
    private String upazila;

    @Size(max = 30, message = "Should not exceed 30 letters")
    private String district;

    public Long getId() {
        return id;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getUpazila() {
        return upazila;
    }

    public void setUpazila(String upazila) {
        this.upazila = upazila;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
