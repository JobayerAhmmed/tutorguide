package com.munshi.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Immutable
@Table(name = "class")
public class Class implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id")
    private Long id;

    @NotNull
    private String name;

    @ManyToOne
    @JoinColumn(name = "medium_id", nullable = false)
    private Medium medium;

    @OneToMany(mappedBy = "className", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Subject> subjects = new HashSet<Subject>();

//    @OneToMany(mappedBy = "className", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private Set<Post> posts = new HashSet<Post>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Medium getMedium() {
        return medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

//    public Set<Post> getPosts() {
//        return posts;
//    }

//    public void setPosts(Set<Post> posts) {
//        this.posts = posts;
//    }
}
