package com.munshi.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Immutable
@Table(name = "area")
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "area_id")
    private Long id;

    @NotNull
    private String name;

//    @OneToMany(mappedBy = "area", fetch = FetchType.LAZY)
//    private Set<Post> posts = new HashSet<Post>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Set<Post> getPosts() {
//        return posts;
//    }

//    public void setPosts(Set<Post> posts) {
//        this.posts = posts;
//    }
}
