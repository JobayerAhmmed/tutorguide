package com.munshi.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "current_address")
public class CurrentAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "current_address_id")
    private Long id;

    @Size(max = 20, message = "Should not exceed 20 letters")
    @Column(name = "house_no")
    private String houseNo;

    @Size(max = 20, message = "Should not exceed 20 letters")
    @Column(name = "road_no")
    private String roadNo;

    @Size(max = 30, message = "Should not exceed 30 letters")
    @Column(name = "area_name")
    private String areaName;

    @Size(max = 30, message = "Should not exceed 30 letters")
    private String city;

    public Long getId() {
        return id;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getRoadNo() {
        return roadNo;
    }

    public void setRoadNo(String roadNo) {
        this.roadNo = roadNo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
