package com.munshi.controller;

import com.munshi.model.AjaxResponse;
import com.munshi.model.Class;
import com.munshi.model.Medium;
import com.munshi.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/post")
public class RestPostController {

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private MediumRepository mediumRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private PostRepository postRepository;

    @GetMapping("/classes")
    public AjaxResponse getClasses(@RequestBody Medium medium) {
        AjaxResponse response = new AjaxResponse();

        if (medium.getId() > 3 && medium.getId() < 1) {
            response.setStatus("");
            response.setData(null);
        }

        List<Class> classList = classRepository.findByMedium(medium);
        response.setStatus("Done");
        response.setData(classList);

        return response;
    }
}
