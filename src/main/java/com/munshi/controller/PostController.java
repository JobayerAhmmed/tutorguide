package com.munshi.controller;

//import com.munshi.model.Post;
//import com.munshi.repository.PostRepository;
import com.munshi.model.Class;
import com.munshi.model.Medium;
import com.munshi.model.Post;
import com.munshi.repository.*;
import com.munshi.service.PostService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class PostController {

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private MediumRepository mediumRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private PostRepository postRepository;

    @GetMapping
    public String index(Model model) {
        List<Post> postList = postRepository.findAll();
        if (postList != null) {
            model.addAttribute("postList", postList);
        }
        return "post/index";
    }

    @GetMapping("/create")
    public String create(Model model) {
        Post post = new Post();
        model.addAttribute("post", post);

        List<Medium> mediumList = mediumRepository.findAll();
        model.addAttribute("mediumList", mediumList);

//        Medium medium = mediumRepository.findOne(1L);
//        List<Class> classList = classRepository.findByMedium(medium);
        return "post/create";
    }

    @PostMapping("/create")
    public String create(@Valid Post post, BindingResult result, Model model) {
        if (result.hasErrors())
            return "post/create";

        postRepository.save(post);
        return "redirect:/";
    }

//    @ModelAttribute("mediumList")
//    public List<Medium> getMediumList() {
//        return mediumRepository.findAll();
//    }

//    @RequestMapping(value = "/{phone}", method = RequestMethod.GET)
//    public String myPosts(@PathVariable("phone") String phone, Model model) {
//        List<Post> postList = repository.findByPhone(phone);
//        if (postList != null) {
//            model.addAttribute("postList", postList);
//        }
//        return "post/myPosts";
//    }
}
