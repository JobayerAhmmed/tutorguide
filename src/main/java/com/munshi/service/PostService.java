package com.munshi.service;

import com.munshi.model.Post;

import java.util.List;

public interface PostService {
    public List<Post> findAllPost();
    public void savePost(Post post);
}
