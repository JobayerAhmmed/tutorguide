package com.munshi.service;

import com.munshi.model.Post;
import com.munshi.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private MediumRepository mediumRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private PostRepository postRepository;

    @Override
    public List<Post> findAllPost() {
        return null;
    }

    @Override
    public void savePost(Post post) {

    }
}
