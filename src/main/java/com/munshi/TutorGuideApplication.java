package com.munshi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorGuideApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorGuideApplication.class, args);
	}
}
